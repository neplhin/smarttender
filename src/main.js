import Vue from 'vue'
import App from './App.vue'
import router from "./router";
import BootstrapVue from 'bootstrap-vue'
import Vuex from 'vuex';
import store from './store/index'
import { firestorePlugin } from 'vuefire'
import firebase from 'firebase/app'
import 'firebase/firestore'
import { VueMaskDirective } from 'v-mask'

import GothamPro from './assets/fonts/gothampro/gothampro.ttf';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/css/styles.css';
require('./assets/css/animate.module.css');


Vue.directive('mask', VueMaskDirective);
Vue.use(firestorePlugin)
firebase.initializeApp({
 projectId: 'smarttender-722d6', 
 databaseURL: "https://smarttender-722d6.firebaseio.com"
})
export const db = firebase.firestore()

Vue.use(Vuex);
Vue.use(BootstrapVue)
Vue.config.productionTip = false;

new Vue({
    router,
    store,
  render: h => h(App),
}).$mount('#app')
//eslint-disable-next-line
console.log(GothamPro)