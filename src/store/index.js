import Vue from "vue";
import Vuex from "vuex";
import smartTender from "./modules/smartTender";


Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    smartTender
  }
});