export default {
  namespaced: true,

  state: {
    smartTender: {},
  },

  mutations: {
    setSmartTenderDirection(state, direction) {
      state.smartTender.direction = direction;
    },
    setSmartTenderCompany(state, company) {
      state.smartTender.company = company;
    },
    setSmartTenderCompanyData(state, data) {
      state.smartTender.data = data;
    },
    setResult(state, res){
      state.smartTender.res = res;
    },
    clear(state){
      state.smartTender = {}
    }
  },

  actions: {
    setUserResult({commit}, res){
      commit("setResult", res);
    },
    setDirection({ commit }, direction) {
        commit("setSmartTenderDirection", direction);
    },
    setCompany({commit}, company) {
      commit("setSmartTenderCompany", company);
    },
    setCompanyData({commit}, data) {
      commit("setSmartTenderCompanyData", data)
    },
    clearSmartTender({commit}){
      commit("clear")
    }
  },
  getters: {
    getSmartTender: state => {
      return state.smartTender;
    },
  }
};
