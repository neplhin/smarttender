import Vue from "vue";
import Router from "vue-router";
import Home from "./pages/Home";

Vue.use(Router);

export default new Router({
    mode: "history",
    base: process.env.BASE_URL,
    routes: [
        {
            path: "/",
            name: "home",
            component: Home
        },
        {
            path: "/page-1",
            name: "page one",
            component: () => import("./pages/Page1.vue")
        },
        {
            path: "/page-2",
            name: "page two",
            component: () => import("./pages/Page2.vue")
        },
        {
            path: "/page-3",
            name: "page three",
            component: () => import("./pages/Page3.vue")
        },
        {
            path: "/page-4",
            name: "page four",
            component: () => import("./pages/Page4.vue")
        },
        {
            path: "/page-5",
            name: "page five",
            component: () => import("./pages/Page5.vue")
        },
        {
            path: "/page-6",
            name: "page six",
            component: () => import("./pages/Page6.vue")
        },
        {
            path: "/page-7",
            name: "page seven",
            component: () => import("./pages/Page7.vue")
        },
        {
            path: "/page-8",
            name: "page eight",
            component: () => import("./pages/Page8.vue")
        },
        {
            path: "/page-9",
            name: "page nine",
            component: () => import("./pages/Page9.vue")
        },
        {
            path: "/page-10",
            name: "page eight",
            component: () => import("./pages/Page10.vue")
        },
        {
            path: "/page-11",
            name: "page nine",
            component: () => import("./pages/Page11.vue")
        },

    ]
});